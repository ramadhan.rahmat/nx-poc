import styled from '@emotion/styled';

interface SharedUiProps {
  name: string;
}

const ContentWrapper = styled.div`
  max-width: 800px;
  margin: 0 auto;
  padding: 20px;
`;

const Title = styled.h1`
  font-size: 24px;
  margin-bottom: 20px;
`;

const Paragraph = styled.p`
  font-size: 16px;
  line-height: 1.5;
`;

export const SharedUi = ({ name }: SharedUiProps) => {
  return (
    <ContentWrapper>
      <Title>Welcome to Our Website | {name}</Title>
      <Paragraph>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam
        velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate
        commodo lectus, ac blandit elit tincidunt id.
      </Paragraph>
      <Paragraph>
        Donec euismod, velit vel feugiat dapibus, augue justo ullamcorper
        turpis, nec convallis metus nunc vel turpis. Nulla sed ligula quam, in
        fermentum ante. Donec varius felis fermentum nisl imperdiet at molestie
        purus porta.
      </Paragraph>
    </ContentWrapper>
  );
};

export default SharedUi;
