import { nxE2EPreset } from '@nx/cypress/plugins/cypress-preset';

import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    ...nxE2EPreset(__filename, {
      cypressDir: 'src',
      webServerCommands: {
        default: 'nx run nx-poc:serve',
        production: 'nx run nx-poc:preview',
      },
      ciWebServerCommand: 'nx run nx-poc:serve-static',
    }),
    baseUrl: 'http://localhost:4200',
  },
});
