import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'feature-remote',

  exposes: {
    './Module': './src/remote-entry.ts',
  },
};

export default config;
