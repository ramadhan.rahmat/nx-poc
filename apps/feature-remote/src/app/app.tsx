import styled from '@emotion/styled';

import NxWelcome from './nx-welcome';

import { SharedUi } from '@nx-poc/shared-ui';

interface AppProps {
  title?: string;
}

const StyledApp = styled.div`
  // Your style here
`;

export function App({ title = '' }: AppProps) {
  return (
    <StyledApp>
      <SharedUi name="Remote App" />
      <NxWelcome title={title} />
    </StyledApp>
  );
}

export default App;
