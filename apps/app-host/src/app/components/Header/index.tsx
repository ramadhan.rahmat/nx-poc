import React from 'react';
import { Container, Logo, Menu, MenuItem } from './style';

const Header = () => {
  return (
    <Container>
      <Logo>Host Header</Logo>
      <Menu>
        <ul>
          <MenuItem>Home</MenuItem>
          <MenuItem>About</MenuItem>
          <MenuItem>Services</MenuItem>
          <MenuItem>Contact</MenuItem>
        </ul>
      </Menu>
    </Container>
  );
};

export default Header;
