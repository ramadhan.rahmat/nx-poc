import styled from '@emotion/styled';

export const Container = styled.header`
  background-color: #796fec;
  border-radius: 16px;
  margin: 8px;
  color: white;
  padding: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Logo = styled.div`
  font-weight: bold;
  font-size: 24px;
`;

export const Menu = styled.nav`
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
  }
`;

export const MenuItem = styled.li`
  margin-left: 20px;
  cursor: pointer;
`;
