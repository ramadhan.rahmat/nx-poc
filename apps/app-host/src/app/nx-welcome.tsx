import Header from './components/Header';

import Module from 'feature-remote/Module';
import { SharedUi } from '@nx-poc/shared-ui';

/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 This is a starter component and can be deleted.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 Delete this file and get started with your project!
 * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
export function NxWelcome({ title }: { title: string }) {
  return (
    <>
      <Header />
      <SharedUi name="From app host" />
      <Module title="modified from app host" />
    </>
  );
}

export default NxWelcome;
