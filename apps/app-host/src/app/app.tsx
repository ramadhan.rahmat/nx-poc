import * as React from 'react';

import NxWelcome from './nx-welcome';

import { Route, Routes } from 'react-router-dom';

export function App() {
  return (
    <React.Suspense fallback={null}>
      <Routes>
        <Route path="/" element={<NxWelcome title="app-host" />} />
      </Routes>
    </React.Suspense>
  );
}

export default App;
