import { nxE2EPreset } from '@nx/cypress/plugins/cypress-preset';

import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    ...nxE2EPreset(__filename, {
      cypressDir: 'src',
      webServerCommands: {
        default: 'nx run feature-remote:serve',
        production: 'nx run feature-remote:preview',
      },
      ciWebServerCommand: 'nx run feature-remote:serve-static',
    }),
    baseUrl: 'http://localhost:4201',
  },
});
